var ServiceBroker = require('./core');

ServiceBrokder.Credentials = {
		
		constructor: function Credentials() {
			ServiceBrokder.util.hideProperties(this, 'secretAccessKey');
			
			if(arguments.length ===1 && typeof arguments[0] === 'object') {
				var creds = arguments[0];
				this.accessKeyId = creds.accessKeyId;
				this.secretAccessKey = creds.secretAccessKey;
			} else {
				this.accessKeyId = arguments[0];
				this.secretAccessKey = arguments[1];
			}
		}	
};