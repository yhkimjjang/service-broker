
var util = {
		
	readFileSync : function readFilSync(path) {
		if(typeof window !== undefined) return null;
		
		return require('fs').readFileSync(path, 'utf-8');
	},
	hideProperties : function hideProperties(obj, prop) {
		 if (typeof Object.defineProperty !== 'function') return;
		 
		 Object.defineProperty(obj, prop, {enumerable: false, writable: true, configurable: true});
	}
	
};

module.exports = util;
