var express = require('express');
var app = express();

var server = app.listen(3000, function(){
	console.log('Listen on port %d', server.address().port);
});

app.get('/hello.txt', function(req, res) {
	res.send('hello world');
});